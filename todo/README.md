# ToDo App

This is a simple ToDo app with simple user login system. Here Tasks can be organized by Task Group.

## Language Used
Backend: JAVA 11
Frontend : Vaadin 23.0.1

## Framework Used
Spring Boot 2.6.4

## Build Tools
Maven

##Project Repository Link
https://zakirho857@bitbucket.org/zakirho857/todo_spring_boot_vaadin.git

## Running the application

The project is a standard Maven project. To run it from the command line,
type `mvnw` (Windows), or `./mvnw` (Mac & Linux), then open
http://localhost:8081 in your browser.

You can also import the project to your IDE of choice as you would with any
Maven project. Read more on [how to import Vaadin projects to different 
IDEs](https://vaadin.com/docs/latest/flow/guide/step-by-step/importing) (Eclipse, IntelliJ IDEA, NetBeans, and VS Code).



## Project structure

- `MainLayout.java` in `src/main/java` contains the navigation setup (i.e., the
  side/top bar and the main menu). This setup uses
  [App Layout](https://vaadin.com/components/vaadin-app-layout).
- `views` package in `src/main/java` contains the server-side Java views of your application.
- `views` folder in `frontend/` contains the client-side JavaScript views of your application.
- `themes` folder in `frontend/` contains the custom CSS styles.


## Some Screen Shorts
<img src="frontend/todo_screen_short/1.PNG"></img>

<img src="frontend/todo_screen_short/2.PNG"></img>

<center><h2>Add Task Group </h2></center>
<img src="frontend/todo_screen_short/3.PNG"></img>

<center><h2>Task Group List </h2></center>
<img src="frontend/todo_screen_short/5.PNG"></img>

<center><h2>Add New Task </h2></center>
<img src="frontend/todo_screen_short/6.PNG"></img>
<center><h2>Task List</h2></center>
<img src="frontend/todo_screen_short/9.PNG"></img>
<center><h2>Task Edit </h2></center>
<img src="frontend/todo_screen_short/7.PNG"></img>
<center><h2>Dashboard</h2></center>
<img src="frontend/todo_screen_short/10.PNG"></img>


## Developer
MD Zakir Hossain,
Sr. Programmer,
Contact: +8801671166491,
email: zakirho857@gmail.com
