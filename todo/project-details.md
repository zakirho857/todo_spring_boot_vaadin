Introduction:
-----------------
This is an App to keep your manage your task using task group easily.

Feature:
-----------
It has a graphical representation of your task group (Project) Wise.
Filter search by Task Group (Project) or name or status

Technology:
--------------
JAVA 11,
Spring Boot 2.6.4,
Spring Security 5,
Vaadin 23.0.1

Developer:
----------------
MD Zakir Hossain.
Email: zakirho857@gmail.com,
Contact: +8801671166491

Database:
---------------
Mysql

Instructions to start:
---------------------
1. Run command npm install in ~todo folder
2. Change Mysql Username and Password as your evironment
3. Import on IDE e.g intelij
4. Run the project