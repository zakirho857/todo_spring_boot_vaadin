package com.zakir.todo.repository;

import com.zakir.todo.entity.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TaskRepository extends JpaRepository<Task, Long> {
    List<Task> findByProgressPercentLessThan(double progressPercent);

    List<Task> findByProgressPercentEquals(double progressPercent);

    List<Task> findByTaskGroup_TaskGroupId(Long taskGroupId);

    @Query(value = "select * from TASK t where lower(t.task_title) like lower(concat('%', :filterText, '%')) ", nativeQuery = true)
    List<Task> getFilteredTasks(String filterText);
}
