package com.zakir.todo.repository;


import com.zakir.todo.entity.TaskGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TaskGroupRepository extends JpaRepository<TaskGroup, Long> {

    @Query(value = "select * from TASK_GROUP g where lower(g.task_group_title) like lower(concat('%', :filterText, '%')) ", nativeQuery = true)
    List<TaskGroup> getFilteredTaskGroups(@Param("filterText")String filterText);
}
