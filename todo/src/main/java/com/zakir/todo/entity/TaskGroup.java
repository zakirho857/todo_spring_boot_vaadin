package com.zakir.todo.entity;

import com.zakir.todo.common.enums.Priority;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Getter
@Setter
public class TaskGroup {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long taskGroupId;
    private String taskGroupTitle;
    private Priority priority;
    private String note;
    private Date dateLimit;
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "taskGroup",cascade = CascadeType.ALL)
    private Set<Task> tasks;

}
