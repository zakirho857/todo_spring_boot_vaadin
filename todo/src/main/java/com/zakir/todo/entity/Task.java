package com.zakir.todo.entity;

import com.zakir.todo.common.enums.Priority;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long taskId;
    private String taskTitle;
    private String description;
    private Date maxDate;
    private Priority priority;
    private Double progressPercent;
    private Date lastUpdDate;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "task_group_id")
    private TaskGroup taskGroup;
}
