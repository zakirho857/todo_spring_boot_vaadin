package com.zakir.todo.exception.handler;

public class AnyExceptionExceptionHandler extends RuntimeException{

	private static final long serialVersionUID = 7835121525123868487L;

	public AnyExceptionExceptionHandler(String message) {
		super(message);
	}
}
