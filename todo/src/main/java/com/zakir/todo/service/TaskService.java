package com.zakir.todo.service;

import com.zakir.todo.common.DateHelper;
import com.zakir.todo.common.ResponseMessage;
import com.zakir.todo.common.enums.Message;
import com.zakir.todo.common.enums.Status;
import com.zakir.todo.dto.TaskDto;
import com.zakir.todo.dto.TaskGroupDto;
import com.zakir.todo.entity.Task;
import com.zakir.todo.entity.TaskGroup;
import com.zakir.todo.repository.TaskGroupRepository;
import com.zakir.todo.repository.TaskRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class TaskService {

    private final TaskRepository taskRepository;
    private final TaskGroupRepository taskGroupRepository;
    private final TaskGroupService taskGroupService;

    public ResponseMessage saveTask(TaskDto taskDto){


        Task task = copyTaskFromDto(taskDto);
      Task savedTask =  taskRepository.save(task);

      if(savedTask == null){
       return  ResponseMessage.builder().success(false).message(Message.FAILED.name()).build();
      }
        return  ResponseMessage.builder().success(true).message(Message.SUCCESS.name()).build();
    }


    public Task copyTaskFromDto(TaskDto taskDto){
        Task task = null;

        if(taskDto.getTaskId() != null){
          Optional<Task> taskOptional = taskRepository.findById(Long.valueOf(taskDto.getTaskId()));
          if(taskOptional.isPresent()){
              task = taskOptional.get();
          }
        }else{
            task = new Task();
        }

        BeanUtils.copyProperties(taskDto, task);
        task.setTaskId(taskDto.getTaskId() == null? null :taskDto.getTaskId().longValue());

        if(task.getProgressPercent() == null){
            task.setProgressPercent(0d);
        }

        task.setLastUpdDate(new Date());

        if(taskDto.getTaskGroup() != null && taskDto.getTaskGroup().getTaskGroupId() != null){
            Optional<TaskGroup> taskGroupOptional = taskGroupRepository.findById(taskDto.getTaskGroup().getTaskGroupId().longValue());

            if(taskGroupOptional.isPresent()){
                task.setTaskGroup(taskGroupOptional.get());
            }

        }

        task.setMaxDate(taskDto.getDateLimit() == null ? null : DateHelper.getDateFromLocalDate(taskDto.getDateLimit()));

        return task;
    }


    public ResponseMessage doneOrUndoneTask(Long taskId, Double taskProgressPercent) {

        boolean successStatus = false;

        Optional<Task> taskOptional = taskRepository.findById(taskId);

        if(taskOptional.isPresent()){
            Task task = taskOptional.get();
            task.setProgressPercent(taskProgressPercent);
           Task updatedTask = taskRepository.save(task);

           if(updatedTask != null){
               successStatus = true;
           }

        }
      return ResponseMessage.builder().success(successStatus).message(Message.FAILED.name()).build();
    }

    public List<TaskDto> getTasksByStatus(Integer taskStatus, String filterText) {

        List<Task> tasks = filterTasks(taskStatus, filterText);

        return tasks.stream().map(task -> copyTaskDtoFromEntity(task)).collect(Collectors.toList());
    }

    private List<Task> filterTasks(Integer taskStatus, String filterText){
        List<Task> tasks = Collections.emptyList();

        if(filterText == null || filterText.isEmpty()){
            if(Status.UNDONE.getStatusCode() == taskStatus){
                tasks = taskRepository.findByProgressPercentLessThan(1);
            }else  if(Status.DONE.getStatusCode() == taskStatus){
                tasks = taskRepository.findByProgressPercentEquals(1);
            }
        }else{
            tasks = taskRepository.getFilteredTasks(filterText);
        }

        return tasks;
    }

    public TaskDto copyTaskDtoFromEntity(Task task){
        TaskDto taskDto = new TaskDto();
        BeanUtils.copyProperties(task, taskDto);
        taskDto.setTaskId(task.getTaskId().intValue());
        taskDto.setPriority(taskDto.getPriority());
        taskDto.setPriority(task.getPriority());
        taskDto.setDateLimit(task.getMaxDate() == null ? null : DateHelper.getLocalDate(task.getMaxDate()));
      //  taskDto.setLastUpdatedDate(DateHelper.convertToStringDate(task.getLastUpdDate(), DateFormat.DD_MM_YYYY.getPattern()));

        if(task.getTaskGroup() != null){
            TaskGroupDto taskGroupDto = new TaskGroupDto();
            taskGroupDto.setTaskGroupId(task.getTaskGroup().getTaskGroupId().intValue());
            taskDto.setTaskGroup(taskGroupService.copyTaskGroupDtoFromEntity(task.getTaskGroup()));
        }
        return taskDto;
    }

    public List<Task> getTasksByTaskGropId(Long taskGroupId) {

        return taskRepository.findByTaskGroup_TaskGroupId(taskGroupId);

    }

    public ResponseMessage deleteTask(long taskId) {

            boolean successStatus = false;

            Optional<Task> taskOptional = taskRepository.findById(taskId);

            if(taskOptional.isPresent()){
                Task task = taskOptional.get();
                taskRepository.delete(task);
                successStatus = true;
            }
            return ResponseMessage.builder().success(successStatus).message(Message.FAILED.name()).build();
        }

    public List<TaskDto> filterTaskByGroupId(Long taskGroupId, Integer statusCode, String filterText) {

        if(taskGroupId != null){
            Collections.emptyList();
        }

        List<Task> tasks = Collections.emptyList();

        List<Task> taskListWithGroupId = taskRepository.findByTaskGroup_TaskGroupId(taskGroupId);
        List<Task> filteredTask = Collections.emptyList();

        if((statusCode != null || filterText != null)) {
            filteredTask = filterTasks(statusCode, filterText);
        }
        filteredTask.retainAll(taskListWithGroupId);

      return  filteredTask.stream().map(task -> copyTaskDtoFromEntity(task)).collect(Collectors.toList());

    }

}
