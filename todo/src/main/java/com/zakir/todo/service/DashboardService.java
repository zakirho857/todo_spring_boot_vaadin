package com.zakir.todo.service;

import com.zakir.todo.entity.TaskGroup;
import com.zakir.todo.repository.TaskGroupRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class DashboardService {

    private final TaskGroupRepository taskGroupRepository;
     private final TaskGroupService taskGroupService;

    public Map<String, Double> getStats() {
        HashMap<String, Double> stats = new HashMap<>();

        List<TaskGroup> unDoneTaskGroups = taskGroupRepository.findAll();

        unDoneTaskGroups.stream().forEach( taskGroup -> {
            Double totalTaskProgressPoint = taskGroup.getTasks().stream().mapToDouble(task -> task.getProgressPercent()).sum();
            stats.put(taskGroup.getTaskGroupTitle(), totalTaskProgressPoint==null ? 0 : taskGroupService.calucateAvgTaskProgressBy(taskGroup));
        });

        return stats;
    }

}
