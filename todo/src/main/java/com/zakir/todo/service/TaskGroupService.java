package com.zakir.todo.service;

import com.zakir.todo.common.DateHelper;
import com.zakir.todo.common.ResponseMessage;
import com.zakir.todo.common.enums.Message;
import com.zakir.todo.dto.TaskGroupDto;
import com.zakir.todo.entity.Task;
import com.zakir.todo.entity.TaskGroup;
import com.zakir.todo.repository.TaskGroupRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class TaskGroupService {

    private final TaskGroupRepository taskGroupRepository;


    public ResponseMessage saveTaskGroup(TaskGroupDto taskGroupDto){
        TaskGroup taskGroup = copyTaskGroupFromDto(taskGroupDto);
      TaskGroup savedTaskGroup =  taskGroupRepository.save(taskGroup);

      if(savedTaskGroup == null){
       return  ResponseMessage.builder().success(false).message(Message.FAILED.name()).build();
      }
        return  ResponseMessage.builder().success(true).message(Message.SUCCESS.name()).build();
    }


    public TaskGroup copyTaskGroupFromDto(TaskGroupDto taskGroupDto){
        TaskGroup taskGroup = null;

        if(taskGroupDto.getTaskGroupId() != null){
          Optional<TaskGroup> taskGroupOptional = taskGroupRepository.findById(Long.valueOf(taskGroupDto.getTaskGroupId()));
          if(taskGroupOptional.isPresent()){
              taskGroup = taskGroupOptional.get();
          }
        }else{
            taskGroup = new TaskGroup();
        }

        BeanUtils.copyProperties(taskGroupDto, taskGroup);
        taskGroup.setTaskGroupId(taskGroupDto.getTaskGroupId() == null ? null : taskGroupDto.getTaskGroupId().longValue());
        taskGroup.setDateLimit(DateHelper.getDateFromLocalDate(taskGroupDto.getDateLimit()));
        return taskGroup;
    }


    public ResponseMessage doneOrUndoneTaskGroup(Long taskGroupId, Double TaskGroupProgressPercent) {

        boolean successStatus = false;

        Optional<TaskGroup> taskGroupOptional = taskGroupRepository.findById(taskGroupId);

        if(taskGroupOptional.isPresent()){
            TaskGroup taskGroup = taskGroupOptional.get();
           TaskGroup updatedTaskGroup = taskGroupRepository.save(taskGroup);

           if(updatedTaskGroup != null){
               successStatus = true;
           }

        }
      return ResponseMessage.builder().success(successStatus).message(Message.FAILED.name()).build();
    }

    public List<TaskGroupDto> getTaskGroups(String filterText) {

        List<TaskGroup> taskGroups;

            if(filterText == null || filterText.isEmpty()){
                taskGroups = taskGroupRepository.findAll();
            }else{
                taskGroups = taskGroupRepository.getFilteredTaskGroups(filterText);
            }

        return taskGroups.stream().map(TaskGroup -> copyTaskGroupDtoFromEntity(TaskGroup)).collect(Collectors.toList());

    }

    public TaskGroupDto copyTaskGroupDtoFromEntity(TaskGroup taskGroup){
        TaskGroupDto taskGroupDto = TaskGroupDto.builder().build();
        BeanUtils.copyProperties(taskGroup, taskGroupDto);
        taskGroupDto.setTaskGroupId(taskGroup.getTaskGroupId().intValue());
        taskGroupDto.setTaskGroupId(taskGroup.getTaskGroupId().intValue());
        taskGroupDto.setDateLimit(DateHelper.getLocalDate(taskGroup.getDateLimit()));
        taskGroupDto.setDateLimit(DateHelper.getLocalDate(taskGroup.getDateLimit()));
        taskGroupDto.setAvgProgressPercent(calucateAvgTaskProgressBy(taskGroup));

        return taskGroupDto;
    }

    public Double getProgressValue(Long taskGroupId, TaskService taskService){

        List<Task> tasks = taskService.getTasksByTaskGropId(taskGroupId);

        if(tasks.isEmpty()){
            return 0d;
        }

        return tasks.stream().mapToDouble(task -> task.getProgressPercent()==null?0d: task.getProgressPercent()).sum()/tasks.size();
    }

    public ResponseMessage deleteTaskGroup(long taskGroupId) {

        boolean successStatus = false;

        Optional<TaskGroup> taskGroupOptional = taskGroupRepository.findById(taskGroupId);

        if(taskGroupOptional.isPresent()){
            TaskGroup taskGroup = taskGroupOptional.get();
            taskGroupRepository.delete(taskGroup);
            successStatus = true;
        }
        return ResponseMessage.builder().success(successStatus).message(Message.FAILED.name()).build();
    }

    public Double calucateAvgTaskProgressBy(TaskGroup taskGroup) {

            Set<Task> tasks = taskGroup.getTasks();
            Double avgTaskProgressPervent = 0d;

            if(tasks.isEmpty()){
                return avgTaskProgressPervent;
            }

            Double totalProgressPercent = tasks.stream().mapToDouble(task -> task.getProgressPercent()).sum();

            avgTaskProgressPervent = totalProgressPercent * 100 / tasks.size();

            return avgTaskProgressPervent;
    }
}
