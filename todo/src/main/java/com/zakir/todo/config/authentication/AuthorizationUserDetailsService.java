/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.zakir.todo.config.authentication;

import com.zakir.todo.user.model.entity.Users;
import com.zakir.todo.user.model.entity.UsersRole;
import com.zakir.todo.user.repository.UsersRepository;
import com.zakir.todo.user.repository.UsersRoleRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author zakir
 */

@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class AuthorizationUserDetailsService implements UserDetailsService{

    public final UsersRepository usersRepository;
    public final UsersRoleRepository userRolesRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        
        Users user = usersRepository.findByUsername(username);
        if (user != null) {
                List<String> roles = new ArrayList<>(); 
                List<UsersRole> roleList = userRolesRepository.findByUsername(user.getUsername());
                roleList.stream().forEach((r) -> {
                roles.add(r.getRoleName());
            });
                user.setRoles(roles);                      
                return user;

            } else {
                throw new RuntimeException("user not found");
            }
        } 
    }
    
    

