/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zakir.todo.user.repository;

import com.zakir.todo.user.model.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;


public interface UsersRepository extends JpaRepository<Users, Long> {

	public Users findByUsernameAndPasswordAndEnabled(String username, String password, boolean enabled);

	public Users findByUsername(String username);

	public Users findByUsernameAndEnabled(String username, boolean enabled);
	
}
