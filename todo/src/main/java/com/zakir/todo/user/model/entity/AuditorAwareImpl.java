package com.zakir.todo.user.model.entity;

import com.zakir.todo.common.util.UserInfoUtils;
import org.springframework.data.domain.AuditorAware;

import java.util.Optional;


public class AuditorAwareImpl implements AuditorAware<String> {

	@Override
	public Optional<String> getCurrentAuditor() {
		return Optional.of(UserInfoUtils.getLoggedInUserName());
	}
}
