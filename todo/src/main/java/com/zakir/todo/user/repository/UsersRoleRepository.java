/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zakir.todo.user.repository;


import com.zakir.todo.user.model.entity.UsersRole;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;


public interface UsersRoleRepository extends JpaRepository<UsersRole, Long>{
	
    public List<UsersRole> findByUsername(String username);

//	UsersRole findByRoleName(String roleName);
   
 
}
