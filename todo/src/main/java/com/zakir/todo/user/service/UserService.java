package com.zakir.todo.user.service;

import com.zakir.todo.common.ResponseMessage;
import com.zakir.todo.common.util.UserInfoUtils;
import com.zakir.todo.dto.UsersDto;
import com.zakir.todo.user.model.entity.Users;
import com.zakir.todo.user.model.entity.UsersRole;
import com.zakir.todo.user.model.response.PostLoginViewInfo;
import com.zakir.todo.user.repository.UsersRepository;
import com.zakir.todo.user.repository.UsersRoleRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;


@Service
@Transactional
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class UserService {

	private final UsersRepository usersRepository;

	private final UsersRoleRepository usersRoleRepository;

	private final UserInfoUtils userInfoUtils;

	private final BCryptPasswordEncoder bCryptPasswordEncoder;
	
	public PostLoginViewInfo postLoginViewInfo() {
		
		PostLoginViewInfo postLoginViewInfo=new PostLoginViewInfo();
		String userName=userInfoUtils.getLoggedInUserName();
		Users user = usersRepository.findByUsername(userName);
		postLoginViewInfo.setUsername(user.getUsername());
		postLoginViewInfo.setNickname(user.getNickName());
		return postLoginViewInfo;
	}

	public ResponseMessage saveUser(UsersDto usersDto) {
		boolean successStatus = false;
		Users user = new Users();
		BeanUtils.copyProperties(usersDto, user);
		user.setEnabled(true);
		user.setPassword(bCryptPasswordEncoder.encode(usersDto.getPassword()));
		UsersRole usersRole = new UsersRole();
		usersRole.setRoleName("ROLE_GUEST");
		usersRole.setUsername(usersDto.getUsername());
		Users savedUser = usersRepository.save(user);
		UsersRole savedUserRole = usersRoleRepository.save(usersRole);

		if(savedUser != null && savedUserRole != null){
			successStatus = true;
		}
	    return ResponseMessage.builder().success(successStatus).build();
	}

	public boolean checkIfUsernameExist(String username){

		boolean usernameExistance = false;

		if(username != null && ! "".equals(username)){
			Users alreadyExistUser = usersRepository.findByUsername(username);

			if(alreadyExistUser != null){
				usernameExistance = true;
			}
		}

		return usernameExistance;
	}

	 
}
