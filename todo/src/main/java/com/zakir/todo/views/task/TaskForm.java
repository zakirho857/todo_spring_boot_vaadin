package com.zakir.todo.views.task;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.zakir.todo.common.MessageViewer;
import com.zakir.todo.common.ResponseMessage;
import com.zakir.todo.common.enums.Priority;
import com.zakir.todo.dto.TaskDto;
import com.zakir.todo.dto.TaskGroupDto;
import com.zakir.todo.service.TaskGroupService;
import com.zakir.todo.service.TaskService;
import com.zakir.todo.views.MainLayout;
import com.zakir.todo.views.common.BeanWriter;
import com.zakir.todo.views.common.ViewDesigner;

import java.util.List;

@PageTitle("Task")
@Route(value = "task-form", layout = MainLayout.class)
public class TaskForm extends FormLayout {

    private IntegerField taskId = new IntegerField();
    private TextField taskTitle = new TextField("Task Title");
    private TextArea description = new TextArea("Description");
    private ComboBox<Priority> priority = new ComboBox<>("Priority");
    private ComboBox<TaskGroupDto> taskGroup = new ComboBox<>("Select Task");
    private DatePicker dateLimit = new DatePicker("Reporting Date");;
    private NumberField progressPercent= new NumberField("Progress Percentage (%)");

    private Button saveBt = ViewDesigner.getSaveButton();
    private Button editBt = ViewDesigner.getEditButton();
    private Button doneBt = ViewDesigner.getDoneButton();
    private Button deleteBt = ViewDesigner.getDeleteButton();
    private Button backBt = ViewDesigner.getBackButton();

    Binder<TaskDto> binder = new BeanValidationBinder<>(TaskDto.class);;

    private final TaskService taskService;

    public TaskForm(TaskGroupService taskGroupService, TaskService taskService){
        this.taskService = taskService;
        setSizeFull();
        setBinderAction();
        initView(taskGroupService);
        addCustomStyle();
        saveBt.setEnabled(binder.isValid());
        setButtonAction();
    }

    public void setBinderAction(){
        binder.bindInstanceFields(this);
        binder.addStatusChangeListener(event -> saveBt.setEnabled(binder.isValid()));
    }

    public void setButtonAction(){
        saveBt.addClickListener(e -> {
            save(taskService);
            UI.getCurrent().getPage().setLocation("tasks");
           // clearContent();
        });

        editBt.addClickListener(e -> {
            changeDataEnableStatus(true);
        });

        doneBt.addClickListener(e -> {
            ResponseMessage responseMessage = taskService.doneOrUndoneTask(taskId.getValue().longValue(),1d);
            MessageViewer.showMessage(responseMessage.isSuccess());
            UI.getCurrent().getPage().setLocation("tasks");
            clearContent();
        });

        deleteBt.addClickListener(e -> {
            ResponseMessage responseMessage = taskService.deleteTask(taskId.getValue().longValue());
            MessageViewer.showMessage(responseMessage.isSuccess());
            UI.getCurrent().getPage().setLocation("tasks");
            clearContent();
        });

        backBt.addClickListener(e -> {
            backToGrid();
        });

    }


    private void backToGrid() {
        UI.getCurrent().getPage().setLocation("tasks");
    }


        public void initView(TaskGroupService taskGroupService){
            taskId.setVisible(false);
            designComponets();
            setDropDowns(taskGroupService);
            add(taskTitle, priority, description, dateLimit, progressPercent, taskGroup, new HorizontalLayout(saveBt,doneBt, deleteBt, backBt));
        }

        public void setDropDowns(TaskGroupService taskGroupService){

            priority.setItems(Priority.values());
            priority.setItemLabelGenerator(Priority::name);

            List<TaskGroupDto> groupDtos = getTaskGroups(taskGroupService);
            taskGroup.setItems(groupDtos);
            taskGroup.setItemLabelGenerator(TaskGroupDto::getTaskGroupTitle);

        }

        private void addCustomStyle(){
            this.getStyle().set("width","98%");
            this.getStyle().set("margin", "auto");
        }


        private void designComponets(){
            ViewDesigner.designTextField(taskTitle, "Enter Task Title","Task Title","e.g To do the work", true, true);
            ViewDesigner.designTextArea(description, "Enter Task Description","Task Description","e.g This is details of task",true);
            ViewDesigner.designComboBox(priority, "Select Priority", "e.g HIGH", false, true);
            ViewDesigner.designDatePicker(dateLimit,"Enter Date Limit", "Date Limit", false, true);
            ViewDesigner.designNumberField(progressPercent, "Enter Task Progress Percent", "Task Progress in Percentage(%)","e.g 20",false, false);
            ViewDesigner.designComboBox(taskGroup, "Select Task Group", "e.g Group 1", false, false);
        }



        public List<TaskGroupDto> getTaskGroups(TaskGroupService taskGroupService){
            return taskGroupService.getTaskGroups(null);
        }

        private TaskDto bindDataToDto(){

        Double progressPercent = 0d;

        TaskDto taskDto = binder.getBean();

        if(taskDto == null){
            BeanWriter writer = new BeanWriter();
            taskDto = TaskDto.builder().build();
            writer.writeBean(binder, taskDto);
        }

        if(this.progressPercent.getValue() != null){
            progressPercent = this.progressPercent.getValue()/100;
        }

       taskDto.setProgressPercent(progressPercent);

        return taskDto;

        }

        private void save(TaskService taskService){
            TaskDto taskDto = bindDataToDto();
            ResponseMessage responseMessage = taskService.saveTask(taskDto);
            MessageViewer.showMessage(responseMessage.isSuccess());
        }

        public void changeDataEnableStatus(boolean status){
            taskTitle.setEnabled(status);
            description.setEnabled(status);
            progressPercent.setEnabled(status);
            priority.setEnabled(status);
            dateLimit.setEnabled(status);
            taskGroup.setEnabled(status);
        }


        public void clearContent(){
        taskId.clear();
        taskTitle.clear();
        description.clear();
        progressPercent.clear();
        priority.clear();
        dateLimit.clear();
        taskGroup.clear();
        }

    public void setTask(TaskDto taskDto){
        binder.setBean(taskDto);
    }

    public void setSaveBtnLabel(String label){
        saveBt.setText(label);
    }

}
