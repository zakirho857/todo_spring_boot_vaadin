package com.zakir.todo.views.common;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;

public class ViewDesigner {

    public static void designTextField(TextField textField, String placeholder, String title, String helperText, boolean isAutoFocus, boolean isRequired){

        designTextField(textField, placeholder, title, helperText, isRequired);
        textField.setAutofocus(isAutoFocus);
    }

    public static void designTextField(TextField textField, String placeholder, String title, String helperText, boolean isRequired){
        designTextField(textField, placeholder, title, helperText);
        textField.setRequired(isRequired);
        textField.setRequiredIndicatorVisible(true);
    }

    public static void designTextField(TextField textField, String placeholder, String title, String helperText){
        designTextField(textField, placeholder, title);
        textField.setHelperText(helperText);
    }


    public static void designTextField(TextField textField, String placeholder, String title){
        textField.setClearButtonVisible(true);
        textField.setRequiredIndicatorVisible(true);
        textField.setTitle(title);
        textField.setPlaceholder(placeholder);
    }


    public static void designNumberField(NumberField NumberField, String placeholder, String title, String helperText, boolean isAutoFocus, boolean isRequired){

        designNumberField(NumberField, placeholder, title, helperText, isRequired);
        NumberField.setAutofocus(isAutoFocus);
    }

    public static void designNumberField(NumberField NumberField, String placeholder, String title, String helperText, boolean isRequired){
        designNumberField(NumberField, placeholder, title, helperText);
        NumberField.setRequiredIndicatorVisible(isRequired);
        NumberField.setRequiredIndicatorVisible(true);
    }

    public static void designNumberField(NumberField NumberField, String placeholder, String title, String helperText){
        designNumberField(NumberField, placeholder, title);
        NumberField.setHelperText(helperText);
    }



    public static void designNumberField(NumberField NumberField, String placeholder, String title){
        NumberField.setClearButtonVisible(true);
        NumberField.setRequiredIndicatorVisible(true);
        NumberField.setTitle(title);
        NumberField.setPlaceholder(placeholder);
    }



    public static void designTextArea(TextArea textArea, String placeholder, String title, String helperText,  boolean isRequired){
        designTextArea(textArea, placeholder,  helperText);
        textArea.setRequired(isRequired);
        textArea.setRequiredIndicatorVisible(true);
    }

    public static void designTextArea(TextArea textArea, String placeholder, String helperText){
        designTextArea(textArea, placeholder);
        textArea.setHelperText(helperText);
    }

    public static void designTextArea(TextArea textArea, String placeholder){
        textArea.setPlaceholder(placeholder);
        textArea.setClearButtonVisible(true);
        textArea.setRequiredIndicatorVisible(true);
    }


    public static void designComboBox(ComboBox comboBox, String placeholder,  String helperText, boolean isAutoFocus, boolean isRequired){
        designComboBox(comboBox, placeholder,  helperText, isAutoFocus);
        comboBox.setRequired(isRequired);
    }

    public static void designComboBox(ComboBox comboBox, String placeholder, String helperText, boolean isAutoFocus){
        designComboBox(comboBox, placeholder, helperText);
        comboBox.setAutofocus(isAutoFocus);
    }


    public static void designComboBox(ComboBox comboBox, String placeholder, String helperText){
        designComboBox(comboBox, placeholder);
        comboBox.setHelperText(helperText);
    }


    public static void designComboBox(ComboBox comboBox, String placeholder){
        comboBox.setClearButtonVisible(true);
        comboBox.setRequiredIndicatorVisible(true);
        comboBox.setPlaceholder(placeholder);
    }

    public static void designDatePicker(DatePicker datePicker, String placeholder, String helperText, boolean isAutoFocus, boolean isRequired){
        designDatePicker(datePicker, placeholder,  helperText, isAutoFocus);
        datePicker.setRequired(isRequired);
        datePicker.setRequiredIndicatorVisible(true);
    }

    public static void designDatePicker(DatePicker datePicker, String placeholder, String helperText, boolean isAutoFocus){
        designDatePicker(datePicker, placeholder, helperText);
        datePicker.setAutoOpen(isAutoFocus);
    }


    public static void designDatePicker(DatePicker datePicker, String placeholder, String helperText){
        designDatePicker(datePicker, placeholder);
        datePicker.setHelperText(helperText);
    }


    public static void designDatePicker(DatePicker datePicker, String placeholder){
        datePicker.setClearButtonVisible(true);
        datePicker.setRequiredIndicatorVisible(true);
        datePicker.setPlaceholder(placeholder);
    }

    public static Button getAddButton(){
        Button addMoreButton = new Button("Add New", VaadinIcon.PLUS_CIRCLE.create());
        return addMoreButton;
    }


    public static Button getSaveButton(){
        Button button = new Button("Save", VaadinIcon.PLUS_CIRCLE.create());
        button.addThemeVariants(  ButtonVariant.LUMO_SUCCESS, ButtonVariant.LUMO_PRIMARY);
        return button;
    }

    public static Button getEditButton(){
        Button button = new Button("Edit", VaadinIcon.EDIT.create());
        button.addThemeVariants(ButtonVariant.LUMO_PRIMARY, ButtonVariant.LUMO_ICON);
        return button;
    }

    public static Button getDoneButton(){
        Button button = new Button("Done", VaadinIcon.CHECK_CIRCLE.create());
        button.getStyle().set("background-color","#17a2b8");
        button.getStyle().set("color", "#fff");
        return button;
    }

    public static Button getDeleteButton(){
        Button button = new Button("Delete", VaadinIcon.TRASH.create());
        button.getStyle().set("background-color","red");
        button.getStyle().set("color", "#fff");
        return button;
    }

    public static Button getBackButton(){
        Button button = new Button("Quit", VaadinIcon.EXIT.create());
        button.getStyle().set("background-color","#FF5733");
        button.getStyle().set("color", "#fff");
        return button;
    }

    public static Button getDetailButton() {
        Button button = new Button(VaadinIcon.SEARCH.create());
        button.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        return button;
    }


    public static TextField getCustomSizeTextField(TextField textField, String maxWidth, String minWidth){
        textField.setMaxWidth(maxWidth);
        textField.setMinWidth(minWidth);
        return textField;
    }

    public static PasswordField getCustomSizePasswordField(PasswordField passwordField, String maxWidth, String minWidth){
        passwordField.setMaxWidth(maxWidth);
        passwordField.setMinWidth(minWidth);
        return passwordField;
    }

    public static PasswordField getCustomSizeTextField(PasswordField passwordField, String maxWidth, String minWidth){
        passwordField.setMaxWidth(maxWidth);
        passwordField.setMinWidth(minWidth);
        return passwordField;
    }

    public static Span getValidationSpan(String validationMessage){
        Span span = new Span();
        span.getStyle().set("color","red");
        span.setText(validationMessage);
        return span;
    }

}
