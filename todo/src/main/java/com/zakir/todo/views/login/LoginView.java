package com.zakir.todo.views.login;

import com.vaadin.flow.component.login.LoginForm;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.*;

@PageTitle("Login")
@Route("login")
@RouteAlias(value = "")

public class LoginView extends VerticalLayout implements BeforeEnterObserver {

	private final LoginForm login = new LoginForm();

	public LoginView(){
		addClassName("login-view");
		setSizeFull();
		setAlignItems(Alignment.CENTER);
		setJustifyContentMode(JustifyContentMode.CENTER);

		VerticalLayout layout = new VerticalLayout();
		layout.setAlignItems(Alignment.CENTER);
		layout.getStyle().set("background-color","#FFF0FE");
		layout.setWidth("380px");
		RouterLink link = new RouterLink("Sign Up", SignupView.class);
		layout.add(login, login, link);
		layout.getStyle().set("margin-top","20px");

		login.setAction("login");
		add(layout);
		login.setForgotPasswordButtonVisible(false);
	}

	@Override
	public void beforeEnter(BeforeEnterEvent beforeEnterEvent) {
		if(beforeEnterEvent.getLocation()
        .getQueryParameters()
        .getParameters()
        .containsKey("error")) {
            login.setError(true);
        }
	}
}