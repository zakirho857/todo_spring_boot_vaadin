package com.zakir.todo.views.taskgroup;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.confirmdialog.ConfirmDialog;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.zakir.todo.common.MessageViewer;
import com.zakir.todo.common.ResponseMessage;
import com.zakir.todo.common.component.ConfirmationDialog;
import com.zakir.todo.common.enums.Priority;
import com.zakir.todo.dto.TaskGroupDto;
import com.zakir.todo.service.TaskGroupService;
import com.zakir.todo.views.MainLayout;
import com.zakir.todo.views.common.ViewDesigner;
import com.zakir.todo.views.common.BeanWriter;

@PageTitle("Task Group")
@Route(value = "task-group-form", layout = MainLayout.class)
public class TaskGroupForm extends FormLayout {

    private TextField taskGroupTitle = new TextField("Task Group Title");;
    private NumberField avgProgressPercent = new NumberField("Average Progress Percent");
    private IntegerField taskGroupId= new IntegerField();;
    private TextArea note= new TextArea("Note");;
    private ComboBox<Priority> priority= new ComboBox<>("Priority");;
    private DatePicker dateLimit = new DatePicker("Reporting Date");;

    private Button saveBt = ViewDesigner.getSaveButton();
    private Button deleteBt =  ViewDesigner.getDeleteButton();
    private Button backBt =  ViewDesigner.getBackButton();

    Binder<TaskGroupDto> binder = new BeanValidationBinder<>(TaskGroupDto.class);;

    public TaskGroupForm(TaskGroupService taskGroupService) {
        initView(taskGroupService);
    }

    public void initView(TaskGroupService taskGroupService) {
        setSizeFull();
        binder.bindInstanceFields(this);
        setDropDowns();
        designComponent();
        setButtonAction(taskGroupService);
        createButtonLayout();
        add(taskGroupTitle, this.priority, dateLimit, avgProgressPercent, note, createButtonLayout());
    }

    private void setDropDowns(){
        this.priority.setItems(Priority.values());
        this.priority.setItemLabelGenerator(Priority::name);
    }

    private void designComponent(){
        ViewDesigner.designTextField(taskGroupTitle, "Enter Task Group Title", "Task Group Title", "e.g. Building Construction", true, true);
        ViewDesigner.designTextArea(note, "Enter Task Group Note", "e.g. 10 stored building of Motijheel, Dhaka", null, false);
        ViewDesigner.designComboBox(this.priority, "Select Priority","e.g HIGH" ,false);
        ViewDesigner.designDatePicker(dateLimit,"Select Deadline Date", "mm-dd.yyyy", false, true);
        ViewDesigner.designNumberField(avgProgressPercent,"Average Progress Percentage", "Average Progess in Percent", "e.g 10%", false, false);
        avgProgressPercent.setEnabled(false);
    }

    private void setButtonAction(TaskGroupService taskGroupService){
        setShortcutKey();
        saveBt.setEnabled(binder.isValid());
        saveBt.addClickListener(click -> save(taskGroupService));
        backBt.addClickListener(click -> backToGrid());

        deleteBt.setEnabled(binder.isValid());
        deleteBt.addClickListener(click -> validateAndDelete(taskGroupService));
        backBt.addClickListener(click -> backToGrid());
        binder.addStatusChangeListener(event -> saveBt.setEnabled(binder.isValid()));
    }

    private void setShortcutKey(){
        saveBt.addClickShortcut(Key.ENTER);
        backBt.addClickShortcut(Key.ESCAPE);
    }

    public void save(TaskGroupService taskGroupService){
        TaskGroupDto taskGroupDto = binder.getBean();
        if(taskGroupDto == null){
            taskGroupDto = TaskGroupDto.builder().build();
            BeanWriter<TaskGroupDto> writer = new BeanWriter<>();
            writer.writeBean(binder, taskGroupDto);
          }
        ResponseMessage responseMessage =  taskGroupService.saveTaskGroup(taskGroupDto);
        MessageViewer.showMessage(responseMessage.isSuccess());
        UI.getCurrent().getPage().setLocation("task-group");

    }


    private void backToGrid() {
       UI.getCurrent().getPage().setLocation("task-group");
    }

    private Component createButtonLayout() {

        return new HorizontalLayout(saveBt, backBt, deleteBt);
    }

    private void validateAndDelete(TaskGroupService taskGroupService) {

        ConfirmDialog dialog = ConfirmationDialog.getDialogue("<h3 style='color:red'>Confirm !!!</h3>", "Are you sure to delete?","Discard");
        Button confirmButton = new Button("Confirm",  VaadinIcon.CHECK.create());
        confirmButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        dialog.setConfirmButton(confirmButton);
        dialog.open();

        confirmButton.addClickListener(event -> {
            if(binder.isValid()){
                ResponseMessage responseMessage = taskGroupService.deleteTaskGroup(binder.getBean().getTaskGroupId());
                MessageViewer.showMessage(responseMessage.isSuccess());
                UI.getCurrent().getPage().setLocation("task-group");
            }
        });
    }


    public void setTaskGroup(TaskGroupDto taskGroupDto){
        binder.setBean(taskGroupDto);
    }


    public void changeDeleteBtnStatus(boolean status){
        this.deleteBt.setEnabled(status);
    }

    public void changeSaveBtnLabel(String label){
        saveBt.setText("Update");
    }

}
