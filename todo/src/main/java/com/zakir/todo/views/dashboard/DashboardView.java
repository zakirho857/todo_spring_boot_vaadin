package com.zakir.todo.views.dashboard;


import com.vaadin.flow.component.charts.Chart;
import com.vaadin.flow.component.charts.model.*;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.zakir.todo.service.DashboardService;
import com.zakir.todo.views.MainLayout;

import java.util.Map;

@PageTitle("Dashboard")
@Route(value = "dashboard", layout = MainLayout.class)
public class DashboardView extends VerticalLayout {

private DashboardService dashboardService;

   public DashboardView(DashboardService dashboardService){
       this.dashboardService = dashboardService;
       setDefaultHorizontalComponentAlignment(Alignment.CENTER);
        add(getTaskStatus(ChartType.PIE),getTaskStatus(ChartType.BAR),getTaskStatus(ChartType.COLUMN),getTaskStatus(ChartType.AREA) );
   }

    private Chart getTaskStatus(ChartType chartType) {
        return makeChart(chartType);
    }

    private Chart makeChart(ChartType chartType){
        Chart chart = new Chart(chartType);
        DataSeries dataSeries = new DataSeries();
        Map<String, Double> taskStatuses = dashboardService.getStats();

        taskStatuses.forEach((taskGroup, proegress) ->{
                    dataSeries.add(new DataSeriesItem(taskGroup, proegress));
          });

        String [] names = new String[taskStatuses.size()];

        int i = 0;
        for(String name : taskStatuses.keySet()){
            names[i] = name;
            i++;
        }
        setGraphCommonStyle(chart, dataSeries, names);
        return chart;
    }

    private void setGraphCommonStyle(Chart chart, DataSeries dataSeries, String[] taskGroupames){
        chart.getConfiguration().setSeries(dataSeries);
        chart.setSizeFull();
        chart.getStyle().set("border","1px solid #BDBDBD");

        XAxis xaxis = new XAxis();
        xaxis.setTitle("Task Group");
        chart.getConfiguration().addxAxis(xaxis);
        LegendTitle title = new LegendTitle();
        title.setText("Progress Status");
        chart.getConfiguration().getLegend().setTitle(title);
        chart.getConfiguration().getxAxis().setCategories(taskGroupames);

    }

  /*  private Chart getAllTaskStatus() {
        Chart chart = new Chart(ChartType.BAR);
        DataSeries dataSeries = new DataSeries();
        Map<String, Integer> companies = dashboardService.getStats();
        companies.forEach((company, employees) ->
                dataSeries.add(new DataSeriesItem(company, employees)));

     *//*   Configuration configuration = chart.getConfiguration();
        configuration.setSeries(dataSeries);*//*
        chart.getConfiguration().setSeries(dataSeries);
        chart.setSizeFull();
        chart.getStyle().set("border","1px solid #BDBDBD");

        XAxis xaxis = new XAxis();
        xaxis.setTitle("Axis title");
        chart.getConfiguration().addxAxis(xaxis);
        //setGraphCommonStyle(chart);
        return chart;
    }

    private Chart getSingleTaskStatus() {
        Chart chart = new Chart(ChartType.COLUMN);
        DataSeries dataSeries = new DataSeries();
        Map<String, Integer> companies = dashboardService.getStats();
        companies.forEach((company, employees) ->
                dataSeries.add(new DataSeriesItem(company, employees)));
        chart.getConfiguration().setSeries(dataSeries);
        chart.getStyle().set("border","1px solid #BDBDBD");

        XAxis xaxis = new XAxis();
        xaxis.setTitle("Axis title");
        chart.getConfiguration().addxAxis(xaxis);
        chart.setSizeFull();
        return chart;
    }

    private Chart getIndividualTaskStatus() {
        Chart chart = new Chart(ChartType.WATERFALL);
        DataSeries dataSeries = new DataSeries();
        Map<String, Integer> companies = dashboardService.getStats();
        companies.forEach((company, employees) ->
                dataSeries.add(new DataSeriesItem(company, employees)));
        chart.getConfiguration().setSeries(dataSeries);
        chart.getStyle().set("border","1px solid #BDBDBD");

        XAxis xaxis = new XAxis();
        xaxis.setTitle("Axis title");
        chart.getConfiguration().addxAxis(xaxis);
        chart.setSizeFull();
        return chart;
    }*/


}