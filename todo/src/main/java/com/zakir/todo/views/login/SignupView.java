package com.zakir.todo.views.login;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouterLink;
import com.zakir.todo.common.MessageViewer;
import com.zakir.todo.common.ResponseMessage;
import com.zakir.todo.common.enums.ConstantMessage;
import com.zakir.todo.dto.UsersDto;
import com.zakir.todo.user.service.UserService;
import com.zakir.todo.views.common.BeanWriter;
import com.zakir.todo.views.common.ViewDesigner;

@PageTitle("Sign Up")
@Route(value = "sign-up")
public class SignupView extends VerticalLayout {

    private TextField username = new TextField("Username");
    private TextField nickName = new TextField("Full Name");;
    private PasswordField password = new PasswordField("Password");
    private PasswordField confPassword = new PasswordField("Confirm Password");
    private Button signupBt = new Button("Signup", VaadinIcon.SIGN_IN.create());
    Binder<UsersDto> binder = new Binder<>(UsersDto.class);
    H1 title = new H1("Sign Up");
    private UserService userService;

    public SignupView(UserService userService){
        this.userService = userService;
        initView();
    }

    private void initView() {
        setWidthFull();
        designCompnent();
        binder.bindInstanceFields(this);
        RouterLink link = new RouterLink("Go For login", LoginView.class);
        add(title, nickName,username,password, confPassword, signupBt, link);
        setJustifyContentMode(JustifyContentMode.AROUND);
        setAlignItems(Alignment.CENTER);
        setActions();
        password.setRequired(true);
        confPassword.setRequired(true);
        binder.addStatusChangeListener(event -> signupBt.setEnabled(getSignupFieldValidationStatus()));
    }

    public void designCompnent(){
        ViewDesigner.getCustomSizeTextField(nickName, "800px", "400px");
        ViewDesigner.getCustomSizeTextField(username, "800px", "400px");
        ViewDesigner.getCustomSizeTextField(password, "800px", "400px");
        ViewDesigner.designTextField(nickName, "Enter Full Name", "Full Name","e.g guest", true, true);
        ViewDesigner.designTextField(username, "Enter Username", "Username","e.g guest", false, true);
        ViewDesigner.getCustomSizePasswordField(confPassword, "800px", "400px" );
        signupBt.addThemeVariants(ButtonVariant.LUMO_SUCCESS, ButtonVariant.LUMO_PRIMARY);
        signupBt.setWidth("200px");
    }

    public void setActions(){
        signupBt.addClickListener(e -> {
            save(userService);
            UI.getCurrent().getPage().setLocation("login");
        });
        username.setValueChangeMode(ValueChangeMode.LAZY);
        showINvalidPasswordMessage();
        showInvalidUserMessage(username.getValue());
        signupBt.setEnabled(binder.isValid());
        enableDisableSignuupBt();
    }

    public void enableDisableSignuupBt(){
        username.addValueChangeListener(event -> showInvalidUserMessage(event.getValue()));
        confPassword.addValueChangeListener(event -> showINvalidPasswordMessage());
        password.addValueChangeListener(event -> checkIfPasswordBlank());
        password.addValueChangeListener(event -> showINvalidPasswordMessage());
    }


    public void checkIfPasswordBlank(){
        if(this.password.equals("")){
            this.password.setHelperComponent(ViewDesigner.getValidationSpan("Password can't be blank"));
        }else{
            this.password.setHelperComponent(null);
        }
    }


    private void showINvalidPasswordMessage() {

        if(this.password.getValue() != "" && confPassword.getValue() != ""){
            if( ! password.getValue().equals(confPassword.getValue())){
                this.confPassword.setHelperComponent(ViewDesigner.getValidationSpan("Confirm Password dosen't match with Password"));
            }else{
                this.confPassword.setHelperComponent(null);
            }
        }

    }

    private boolean doesPasswordMatch(){

        boolean matchStatus = false;

        if(password.getValue() != null && confPassword.getValue() != null){
            if( ! password.getValue().equals(this.confPassword.getValue())){
                matchStatus = false;
            }else{
                matchStatus = true;
            }
        }

        return matchStatus;
    }

    private boolean hasFullName(){
        return nickName.getValue() != null && ! "".equals(nickName.getValue());
    }

    private boolean getSignupFieldValidationStatus(){
        return hasFullName() && binder.isValid() && doesPasswordMatch() && ! userService.checkIfUsernameExist(this.username.getValue());
    }


    private void showInvalidUserMessage(String username){

       boolean usernameExistanceStatus = userService.checkIfUsernameExist(username);

       if(usernameExistanceStatus){
          this.username.setHelperComponent(ViewDesigner.getValidationSpan(ConstantMessage.DUPLICATION_MSG));
       }else{
           this.username.setHelperComponent(null);
       }

    }


    private void save(UserService userService){
        UsersDto usersDto = bindDataToDto();
        ResponseMessage responseMessage = userService.saveUser(usersDto);
        MessageViewer.showMessage(responseMessage.isSuccess());
    }

    private UsersDto bindDataToDto(){
        UsersDto usersDto = binder.getBean();
        BeanWriter writer = new BeanWriter();
        usersDto = UsersDto.builder().build();
        writer.writeBean(binder, usersDto);
        return usersDto;
    }

}
