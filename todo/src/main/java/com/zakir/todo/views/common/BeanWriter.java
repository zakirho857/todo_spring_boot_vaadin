package com.zakir.todo.views.common;


import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationException;

public class BeanWriter<T> {

    public boolean writeBean(Binder<T> binder, T obj){
        try{
            if(binder.isValid()){
                binder.writeBean(obj);
                return true;
            }else {
                return false;
            }

        }catch (ValidationException e){
            e.printStackTrace();
            return false;
        }
    }
}
