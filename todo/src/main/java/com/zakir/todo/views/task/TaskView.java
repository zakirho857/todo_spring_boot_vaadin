package com.zakir.todo.views.task;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.progressbar.ProgressBar;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.zakir.todo.common.enums.Status;
import com.zakir.todo.dto.TaskDto;
import com.zakir.todo.dto.TaskGroupDto;
import com.zakir.todo.service.TaskGroupService;
import com.zakir.todo.service.TaskService;
import com.zakir.todo.views.MainLayout;
import com.zakir.todo.views.common.ViewDesigner;

import java.util.Arrays;
import java.util.List;

@PageTitle("Tasks")
@Route(value = "tasks", layout = MainLayout.class)
public class TaskView extends VerticalLayout {

    private TextField filterText = new TextField("Search");
    private Grid<TaskDto> grid = new Grid<>(TaskDto.class);
    private List<TaskDto> tasks;
    private Button addMoreButton = new Button("Add New");
    private ComboBox<Status> taskStatusDrop = new ComboBox<>("Task Status");
    private ComboBox<TaskGroupDto> taskGroupDrop = new ComboBox<>("Task Group");
    private TaskForm taskForm;
    private Div content = new Div();
    private TaskService taskService;
    private TaskGroupService taskGroupService;

    private Binder binder = new BeanValidationBinder<>(TaskDto .class);

    public TaskView(TaskGroupService taskGroupService, TaskService taskService){
        setSizeFull();
        initView(taskGroupService, taskService);

    }

    private void initView(TaskGroupService taskGroupService, TaskService taskService){
        this.taskService = taskService;
        this.taskGroupService = taskGroupService;
        this.taskForm = new TaskForm(taskGroupService, taskService);
        this.taskForm.setVisible(false);
        setSizeFull();
        initComponent();
        setActions();
        add(new HorizontalLayout(filterText,taskStatusDrop, taskGroupDrop, addMoreButton), content);
        closeEditor();
    }

    private void setActions(){

        addMoreButton.addClickListener(e ->  openEditor());
        taskStatusDrop.addValueChangeListener(e -> {
            grid.setItems(taskService.getTasksByStatus(e.getValue().getStatusCode(), filterText.getValue()));
        });

        grid.addItemClickListener(event ->  {

            if( event.getColumn().getId().get().equals("detailBt")){
                TaskDto dto = event.getItem();
                dto.setProgressPercent(dto.getProgressPercent() * 100);
                editTask(dto);
                taskForm.setSaveBtnLabel("Update");
            }
        });

        filterText.addValueChangeListener(e -> configureFilter());

        taskGroupDrop.addValueChangeListener(e -> fiterWithTaskGroup());
    }

    private void fiterWithTaskGroup() {

        List<TaskDto> taskDtos = taskService.filterTaskByGroupId(taskGroupDrop.getValue().getTaskGroupId().longValue(), taskStatusDrop.getValue().getStatusCode(), filterText.getValue());
        grid.setItems(taskDtos);
    }


    private void initComponent(){
        addMoreButton = ViewDesigner.getAddButton();
        addMoreButton.getStyle().set("margin-top","35px");
        filterText.setValueChangeMode(ValueChangeMode.LAZY);
        filterText.setPrefixComponent(VaadinIcon.SEARCH.create());

        taskStatusDrop.setItems(Arrays.asList(Status.values()));
        taskStatusDrop.setItemLabelGenerator(Status::name);
        taskStatusDrop.setValue(Status.UNDONE);

        List<TaskGroupDto> taskGroups = taskGroupService.getTaskGroups(null);
        taskGroupDrop.setItems(taskGroups);
        taskGroupDrop.setItemLabelGenerator(TaskGroupDto::getTaskGroupTitle);


        grid = new Grid<>(TaskDto.class);
        configureGrid();
        content = new Div(grid, this.taskForm);
        content.setSizeFull();

        HorizontalLayout topbar = new HorizontalLayout();
        topbar.setJustifyContentMode(JustifyContentMode.END);
        addMoreButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);

        designComponents();

        topbar.add(filterText, taskStatusDrop, addMoreButton);

    }

    public void designComponents(){
        ViewDesigner.designTextField(filterText,"Search","");
        ViewDesigner.designComboBox(taskStatusDrop, "Task Status", "e.g Done / Undone", false, true);
        ViewDesigner.designComboBox(taskGroupDrop, "Select Task Group", "e.g group 1", false, true);

    }


    private void configureFilter() {
        String givenText = this.filterText.getValue();
        grid.setItems(taskService.getTasksByStatus(taskStatusDrop.getValue()==null ? null : taskStatusDrop.getValue().getStatusCode(), givenText));
    }

    public void configureGrid(){

        grid.setColumns("taskId","taskTitle","description","priority","dateLimit");
        tasks = getTasks(taskService, Status.UNDONE.getStatusCode());
        grid.setItems(tasks);
        grid.setSizeFull();
        grid.addColumn(e -> e.getTaskGroup() == null ? "-" : e.getTaskGroup().getTaskGroupTitle()) .setHeader("Task Group");

        grid.getColumnByKey("taskId").setVisible(false);
        grid.getColumns().forEach(e -> e.setAutoWidth(true));
        grid.addComponentColumn(e -> getProgressstatus(e.getProgressPercent())).setHeader("Progress Status");
        grid.addComponentColumn(e -> ViewDesigner.getDetailButton()).setHeader("Details").setId("detailBt");
        grid.getColumns().stream().forEach(column -> column.setAutoWidth(true));

    }

    private void editTask(TaskDto taskDto) {

        if(taskDto == null){
            closeEditor();
        }else{
            taskForm.setTask(taskDto);
            openEditor();
        }
    }

    private void closeEditor() {
        grid.setVisible(true);
        taskForm.setVisible(false);
        taskGroupDrop.setVisible(true);
    }


    private void openEditor(){
        grid.setVisible(false);
        taskForm.setVisible(true);
        taskForm.setSizeFull();
        filterText.setVisible(false);
        addMoreButton.setVisible(false);
        taskStatusDrop.setVisible(false);
        taskGroupDrop.setVisible(false);
    }


    public List<TaskDto> getTasks(TaskService taskService, Integer statusCode){
        return taskService.getTasksByStatus(statusCode, filterText.getValue());
    }

    private ProgressBar getProgressstatus(Double value) {
        ProgressBar progressBar = new ProgressBar();
        progressBar.setValue(value);
        return progressBar;
    }


    }
