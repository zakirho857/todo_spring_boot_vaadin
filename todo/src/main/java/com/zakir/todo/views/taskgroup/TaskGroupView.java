package com.zakir.todo.views.taskgroup;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.progressbar.ProgressBar;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.zakir.todo.dto.TaskGroupDto;
import com.zakir.todo.service.TaskGroupService;
import com.zakir.todo.service.TaskService;
import com.zakir.todo.views.MainLayout;
import com.zakir.todo.views.common.ViewDesigner;


@PageTitle("Task Group")
@Route(value = "task-group", layout = MainLayout.class)
public class TaskGroupView extends VerticalLayout {

    private TaskGroupService taskGroupService;
    private TaskService taskService;
    private TaskGroupForm taskGroupForm;
    private Grid<TaskGroupDto> grid = new Grid<>(TaskGroupDto.class);
    private TextField filterText = new TextField("Filter By Group Name");
    Binder binder = new BeanValidationBinder<>(TaskGroupDto.class);
    private Div content;
    private Button addNewBt = new Button("Add New", VaadinIcon.PLUS_CIRCLE.create());


    public TaskGroupView(TaskGroupService taskGroupService, TaskService taskService){
        initView(taskGroupService, taskService);
    }

    void initView(TaskGroupService taskGroupService, TaskService taskService){
        this.taskGroupService = taskGroupService;
        this.taskService = taskService;
        this.taskGroupForm = new TaskGroupForm(taskGroupService);
        setSizeFull();
        configureGrid();
        configureFilter();
        initComponent();
        add(new HorizontalLayout(filterText,addNewBt), content);
        closeEditor();
    }

    private void initComponent(){
        content = new Div(grid, taskGroupForm);
        content.setSizeFull();
        ViewDesigner.designTextField(filterText, "Search ..", "Search By Group Name");
        filterText.addValueChangeListener(e -> configureFilter());
        filterText.setPrefixComponent(VaadinIcon.SEARCH.create());
        addNewBt.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        addNewBt.getStyle().set("margin-top","35px");
        addNewBt.addClickListener(e ->  openEditor());
    }


    private void closeEditor() {
        taskGroupForm.setTaskGroup(null);
        taskGroupForm.setVisible(false);
    }


    private void configureGrid() {
            grid.setSizeFull();
            grid.setColumns("taskGroupId","taskGroupTitle","priority","dateLimit","avgProgressPercent","note");
            grid.getColumnByKey("taskGroupId").setVisible(false);
            grid.getColumns().forEach(e -> e.setAutoWidth(true));

            grid.addComponentColumn(e -> getProgressstatus(taskGroupService.getProgressValue(e.getTaskGroupId() == null? null : Long.valueOf(e.getTaskGroupId()), taskService))).setHeader("Progress Status");
            grid.addComponentColumn(e -> ViewDesigner.getDetailButton()).setHeader("Details").setId("detailBt");
            grid.getColumns().stream().forEach(column -> column.setAutoWidth(true));
            grid.addItemClickListener(event ->  {
                if( event.getColumn().getId().get().equals("detailBt")){
                        editTaskGroup(event.getItem());
                }
            });

    }

    private void editTaskGroup(TaskGroupDto taskGroupDto) {

        if(taskGroupDto == null){
            closeEditor();
        }else{
            grid.setVisible(false);
            taskGroupForm.setTaskGroup(taskGroupDto);
            taskGroupForm.setVisible(true);
            taskGroupForm.setSizeFull();
            taskGroupForm.changeDeleteBtnStatus(true);
            taskGroupForm.changeSaveBtnLabel("Update");
            filterText.setVisible(false);
            addNewBt.setVisible(false);

        }
    }

    private void openEditor(){
        grid.setVisible(false);
        taskGroupForm.setVisible(true);
        taskGroupForm.setSizeFull();
        filterText.setVisible(false);
        addNewBt.setVisible(false);
    }

    private void configureFilter(){
        grid.setItems(taskGroupService.getTaskGroups(filterText.getValue()));
    }

    private ProgressBar getProgressstatus(Double value) {
        ProgressBar progressBar = new ProgressBar();
        progressBar.setValue(value);
        return progressBar;
    }

}
