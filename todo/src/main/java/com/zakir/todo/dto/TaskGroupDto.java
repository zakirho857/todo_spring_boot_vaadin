package com.zakir.todo.dto;

import com.zakir.todo.common.enums.ConstantMessage;
import com.zakir.todo.common.enums.Priority;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TaskGroupDto {

    private Integer taskGroupId;
    @NotBlank(message = ConstantMessage.NOT_BLANK_MSG)
    private String taskGroupTitle;
    @NotNull(message = ConstantMessage.NOT_BLANK_MSG)
    private Priority priority;
    private Integer taskGroupStatus;//1 for done null or 0 for undone
    @NotNull(message = ConstantMessage.NOT_BLANK_MSG)
    private LocalDate dateLimit;
    private String note;
    private Double avgProgressPercent;

}
