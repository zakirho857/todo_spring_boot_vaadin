package com.zakir.todo.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
public class TestDto {

    @NotBlank(message = "Name can't be empty")
    @Size(max = 6, min = 2, message = "Name must be 2 to 6 characters")
    private String name;


}
