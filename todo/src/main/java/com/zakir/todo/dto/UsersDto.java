package com.zakir.todo.dto;


import com.zakir.todo.common.enums.ConstantMessage;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.validation.constraints.NotBlank;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UsersDto{

	private Long userId;

	@NotBlank(message = ConstantMessage.NOT_BLANK_MSG)
	private String username;

	@NotBlank(message = ConstantMessage.NOT_BLANK_MSG)
	private String nickName;

	@NotBlank(message = ConstantMessage.NOT_BLANK_MSG)
	private String password;

	@NotBlank(message = ConstantMessage.NOT_BLANK_MSG)
	private String confPassword;
}
