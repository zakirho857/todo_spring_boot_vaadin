package com.zakir.todo.dto;

import com.zakir.todo.common.enums.ConstantMessage;
import com.zakir.todo.common.enums.Priority;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TaskDto {
    private Integer taskId;
    @NotBlank(message = ConstantMessage.NOT_BLANK_MSG)
    @Size(max = 100, min = 3, message = ConstantMessage.LENGTH_CROSS_MSG)
    private String taskTitle;
    @NotBlank(message = ConstantMessage.NOT_BLANK_MSG)
    @Size(max = 200, min = 10, message = ConstantMessage.LENGTH_CROSS_MSG)
    private String description;

    @NotNull(message = ConstantMessage.NOT_BLANK_MSG)
    private Priority priority;

    @NotNull(message = ConstantMessage.NOT_BLANK_MSG)
    private Double progressPercent;

    @NotNull(message = ConstantMessage.NOT_BLANK_MSG)
    private LocalDate dateLimit;

     private TaskGroupDto taskGroup;

}
