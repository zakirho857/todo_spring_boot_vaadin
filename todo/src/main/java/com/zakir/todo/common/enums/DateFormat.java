package com.zakir.todo.common.enums;

public enum DateFormat {

    DD_MM_YYYY("dd/MM/yyyy");

    private final String pattern;

    DateFormat(String pattern){
        this.pattern = pattern;
    }

    public String getPattern() {
        return pattern;
    }
}
