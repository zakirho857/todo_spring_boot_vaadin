package com.zakir.todo.common.component;

import com.vaadin.flow.component.Html;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.confirmdialog.ConfirmDialog;
import com.vaadin.flow.component.icon.VaadinIcon;

public class ConfirmationDialog{

    public static ConfirmDialog getDialogue(String headerHtml, String confirmationMessage,  String discardText){

        ConfirmDialog dialog = new ConfirmDialog();

        Html span = new Html(headerHtml);

        dialog.setHeader(span);
        dialog.setText(confirmationMessage);
        dialog.setRejectable(true);
        dialog.setRejectText(discardText);
        dialog.setCancelable(true);

        Button cancelButton = new Button("Cancel");
        Button rejectButton = new Button("Cancel", VaadinIcon.EXIT.create());

        dialog.setCancelButton(cancelButton);
        dialog.setCancelButton(rejectButton);

        return dialog;
    }

}
