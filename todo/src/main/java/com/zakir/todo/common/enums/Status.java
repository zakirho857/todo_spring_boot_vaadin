package com.zakir.todo.common.enums;

public enum Status {
    DONE(1), UNDONE(0);

    private final Integer statusCode;

    Status(Integer statusCode){
        this.statusCode = statusCode;
    }

    public Integer getStatusCode() {
        return statusCode;
    }
}
