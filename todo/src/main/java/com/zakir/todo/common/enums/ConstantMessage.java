package com.zakir.todo.common.enums;

public class ConstantMessage {

   public static final String NOT_BLANK_MSG = "This Field Can't be blank";
   public static final String LENGTH_CROSS_MSG = "Value has disobeyed character range";
   public static final String DUPLICATION_MSG = "Already Exist";

}
