package com.zakir.todo.common;

import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.zakir.todo.common.enums.Message;

public class MessageViewer {

    public static void showMessage(boolean messageStatus){
        if(messageStatus){
            Notification notification = Notification.show(Message.SUCCESS.name());
            notification.addThemeVariants(NotificationVariant.LUMO_SUCCESS);
        }else{
            Notification notification = Notification.show(Message.FAILED.name());
            notification.addThemeVariants(NotificationVariant.LUMO_ERROR);
        }

    }

    public static void showMessage(String messages){
            Notification notification = Notification.show(messages);
            notification.addThemeVariants(NotificationVariant.LUMO_ERROR);

    }
}
