package com.zakir.todo.common.response;

import lombok.*;

/**
 * 
 * @author Zakir
 *
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ErrorResponse {
	
	private String className;
	private String methodName;
	private int lineNo;
	private String message;
	private String ipAddress;
	private String uri;
	private String error;
	private String exceptionClass;
	private String exceptionDetails;

}
