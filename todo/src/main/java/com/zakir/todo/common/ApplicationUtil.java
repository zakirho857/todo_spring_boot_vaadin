package com.zakir.todo.common;

import com.zakir.todo.common.enums.Priority;

public class ApplicationUtil {


    public static String getPriorityLevelByCode(Integer priorityCode){

        String priorityLevel = null;

        if(priorityCode == null || priorityCode==0){
            return priorityLevel;
        }

        for(Priority priority : Priority.values()){
            if(priority.getPriorityCode().equals(priorityCode)){
                return priority.name();
            }
        }
        return priorityLevel;
    }

}
