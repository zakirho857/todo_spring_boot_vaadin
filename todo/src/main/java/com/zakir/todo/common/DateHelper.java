package com.zakir.todo.common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

public class DateHelper {

    public static String convertToStringDate(Date date, String pattern){
        String formattedDate = "";
        if(date == null || pattern == null || pattern.isEmpty()){
            return formattedDate;
        }
        return new SimpleDateFormat(pattern).format(date);
    }

    public static Date parseToDate(String date, String pattern){
        Date formattedDate = null;
        if(date == null || pattern == null || pattern.isEmpty()){
            return formattedDate;
        }
        try {
            return new SimpleDateFormat(pattern).parse(date);
        }catch (ParseException e){
            e.printStackTrace();
        }

        return formattedDate;
    }

    public static LocalDate getLocalDate(String date, String datePattern){

        LocalDate localDate = null;

        if(date == null || date.isEmpty() || datePattern == null || datePattern.isEmpty()){
            return localDate;
        }

        Date utilDate = parseToDate(date, datePattern);

        if(utilDate != null){
            localDate =  utilDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        }

       return localDate;

    }

    public static LocalDate getLocalDate(Date date){

        LocalDate localDate = null;

        if(date == null){
            return localDate;
        }

        if(date != null){
            localDate =  date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        }

        return localDate;

    }

    public static Date getDateFromLocalDate(LocalDate localDate){
        if(localDate == null){
            return null;
        }
     return  Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }
}
