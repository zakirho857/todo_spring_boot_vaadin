package com.zakir.todo.common;

import com.vaadin.flow.component.grid.Grid;

import java.util.Optional;
import java.util.function.Consumer;

public class GridHelper {

    public static <T> void addColumnClickListener(Grid<T> grid,
                                                  Consumer<Grid.Column<T>> listener) {
        String expression = "function(){const col=element.getEventContext(event).column;return col ? col.id : '';}()";
        grid.getElement().addEventListener("click", e -> {
            String colId = e.getEventData().getString(expression);
            Optional<Grid.Column<T>> column = grid.getColumns().stream()
                    .filter(col -> colId.equals(col.getId().get())).findFirst();
            column.ifPresent(listener);
        }).addEventData(expression);
    }
}
