package com.zakir.todo.common.enums;

public enum Priority {

    HIGH(1), MEDIUM(2),LOW(3);

    private final Integer priorityCode;

    Priority(Integer priorityCode){
        this.priorityCode = priorityCode;
    }

    public Integer getPriorityCode() {
        return priorityCode;
    }
}
